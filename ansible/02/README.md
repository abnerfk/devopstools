# Provisionar vm usando Ansible
vagrant box add geerlingguy/centos7
vagrant init geerlingguy/centos7
vagrant up
vagrant ssh

# Remover máquina
vagrant destroy
