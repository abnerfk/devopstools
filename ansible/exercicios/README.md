# Exercícios Ansible

## Setup para uso da VM principal para acesso as VM's do exercício

### Passos que devem ser executados na sua máquina

Criar máquinas para os exercícios acessando a pasta `ansible/02` dentro do repositório devopstools que vocês clonaram. Lembrando que para atualizar a sua cópia local do repositório você deve executar `git pull` dentro do diretório onde você clonou o repositório.

Após atualizar o repositório você deve executar o comando abaixo de dentro do diretório do repositório clonado:

```
cp ~/.vagrant.d/insecure_private_key .
```

### Passos que devem ser executados na VM do treinamento (tt)

Agora dentro davm do treinamento (vm tt) você deve criar o diretório onde o arquivo deverá ser copiado e então enviar o arquivo para o novo diretório:

```
mkdir ~/.vagrant.d
cp /vagrant/insecure_private_key ~/.vagrant.d/
```

Após configurar a chave de acesso as máquinas você deve acessar o diretório que tem a configuração das máquinas para poder executar os exercícios:

```
cd /vagrant/ansible/02
```

### Todos os exercícios devem ser executados a partir da vm do treinamento

## Comandos ad-hoc

Executar os comandos abaixo e verificar os resultados:

```
ansible -m ping all
ansible -m ping db
ansible multi -b -m yum -a "name=chrony state=present"
ansible multi -b -m service -a "name=chronyd state=started enabled=yes"
ansible app -b -m yum -a "name=python3-pip state=present"
ansible app -b -m pip -a "name=django<4 state=present"
ansible app -a "python -m django --version"
ansible db -b -m yum -a "name=mariadb-server state=present"
ansible db -b -m service -a "name=mariadb state=started enabled=yes"
ansible multi -m copy -a "src=/etc/hosts dest=/tmp/hosts"
```

## Playbook 1 - app1 e app2
Criar playbook para instalar e ativar o nginx nos servidores de aplicação (app1 e app2)
Nome do pacote: nginx

## Playbook 2 - app1 e app2
Complementar playbook anterior criando uma página inicial para o nginx com o conteúdo abaixo:

```
<html>
  <head>
    <title>Treinamento DevOps TT</title>
  <head>
  <body>
    <h1> Página inicial criada com o Ansible </h1>
  </body>
</html>
```

Este arquivo deve ser criado como index.html dentro da pasta /usr/share/nginx/html com o usuário root sendo dono do arquivo e as permissões devem ser 0644.

## Playbook 3 - app1 e app2
Alterar playbook para inserir o hostname da máquina na página que foi criada no playbook anterior.

Deve-se criar um template e então usar a variável {{ inventory_hostname }} no template abaixo:

```
<html>
  <head>
    <title>Treinamento DevOps TT</title>
  <head>
  <body>
    <h1> Página inicial criada com o Ansible </h1>
    <h2> Servidor: {{ inventory_hostname }} </h2>
  </body>
</html>
```

Este arquivo dever ser salvo como index.html.j2 e deve ser salvo como index.html na pasta /usr/share/nginx/html com o dono root e as permissões 0644.

## Role 1 - app1 e app2
Transformar playbook anterior em uma role chamada tt-nginx e aplicar nos sevidores de aplicação

## Role 2 -  todos os hosts
Criar role que instala e ativa o docker nos servidores

## Pesquisar e instalar role - todos hosts
Pesquisar no ansible galaxy uma role que instala e ativa do docker nos servidores, baixar a role e aplicar em todos os hosts.

> Sugestão: https://galaxy.ansible.com/geerlingguy/docker

### Links úteis
[Criar playbook](http://ansible-br.org/primeiros-passos/guia-rapido/passo-6/)

[Criando Roles](https://ansible.github.io/workshops/exercises/ansible_rhel/1.7-role/README.pt-br.html#passo-2---criando-uma-estrutura-b%C3%A1sica-de-diret%C3%B3rio-de-roles)

[Criando tasks da role](https://ansible.github.io/workshops/exercises/ansible_rhel/1.7-role/README.pt-br.html#passo-3---criando-o-arquivo-de-tasks)

[Criando handler](https://ansible.github.io/workshops/exercises/ansible_rhel/1.7-role/README.pt-br.html#passo-4---criando-o-handler)

[Usar roles em playbooks](https://ansible.github.io/workshops/exercises/ansible_rhel/1.7-role/README.pt-br.html#passo-6---teste-a-role)
