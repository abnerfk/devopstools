# Rodando AWX para testes

```
minikube start --cpus=4 --memory=8g --addons=ingress
kubectl apply -f https://raw.githubusercontent.com/ansible/awx-operator/0.8.0/deploy/awx-operator.yaml
kubectl get pods
```

```
cat myawx.yml
---
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx
spec:
  tower_ingress_type: Ingress
```

```
kubectl apply -f myawx.yml
minikube service awx-service --url
kubectl get secret awx-admin-password -o jsonpath='{.data.password}' | base64 --decode
```