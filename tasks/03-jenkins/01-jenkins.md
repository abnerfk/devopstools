# Jenkins + Gitlab Execises

### Importar projeto no Gitlab

1. Logar no Gitlab usando a sua conta
2. Clicar na opção `New Project`
3. Selecionar a opção `Import project/repository`
4. Selecionar a opção `Repo by URL`
5. Inserir a seguinte URL no campo `GIT repository URL`: `https://github.com/amioranza/app_go.git`
6. No campo `Project name` mantenha o nome que apareceu, no campo `Project slug` mude o valor para somente `app`
7. Em `Visibility Level` mantena a opção priivado.
8. Clique em `Create project`

### Configuração inicial do Jenkins
1. Na tela inicial do Jenkins acessar a opção Credentials
2. Em Stores scoped to Jenkins selecione (global)
3. Clique em Add Credentials
4. Mantenha as opções Kind e Scope como estão (Username with password / Global)
5. Em Username insira seu usuário do gitlab.com
6. Em Password coloque a senha da sua conta do Gitlab
7. Em Description coloque Credenciais do Gitlab
8. Clique em OK.


### Criar pipeline no Jenkins

1. Copiar a URL de clone do seu projeto app no GitLab
2. Criar no jenkins um novo item do tipo pipeline com nome testapp
3. Em Description insira Test Application
4. Na parte do Pipeline > Definition selecione Pipeline script from SCM
5. SCM selecione Git
6. Em repository URL coloque a URL HTTP de clone do projeto que consta no Gitlab obtida no passo 1.
7. Em Credentials selecione a credencial criada no passo de configuração inicial do Jenkins.
8. Em Branches to build mantenha com a opção */master
9. Repository Browser mantenha como (Auto)
10. Manteha Script Path como está: Jenkinsfile
11. Clique em Save
12. Faça o clone do repositório do projeto usando a URL do Gitlab, url obtida no passo 1.
13. Acesse o diretório do repositório
14. Crie uma arquivo vazio chamado Jenkinsfile dentro da pasta testapp: `touch Jenkinsfile`
15. Insira o conteúdo abaixo no arquivo Jenkinsfile:

```
pipeline {
	agent any
    
    parameters {
        string(
            name: 'Parametro1',
            defaultValue: '',
            description: 'Exenplo de parametro.'
        )
    }
    
    environment {
        var1 = "EXEMPLO DE VARIAVEL"
    }

    stages {
        stage('Pergunta') {
            input {
                message "Devo continuar?"
                ok "Sim, deve."
                parameters {
                    string(name: 'PERSON', defaultValue: 'Sr Jenkins', description: 'Para quem eu digo olá?')
                }
            }
            steps {
                echo "Olá, ${PERSON}, prazer em conhecê-lo."
            }
        }

        stage ("Prepare") {
		    when {
		        expression {
		            return "${params.Parametro1}" != '';
		        }
		    }
        	steps {
                sh("echo ${var1}")
                sh("ls -Rla")
                sh("hostname")
                sh("ping -c10 google.com")
                sh("[ -e /tmp/teste ] || mkdir /tmp/teste")
                sh("echo ${params.Parametro1}")
            }
        }
        stage ("Validate") {
		    when {
		    	expression {
		        	return "${params.Parametro1}" != '';
		        }
		    }
		    steps {
				sh("echo Acabou o Pipeline!!!!!")
	        }
        }
	}
}
```

16. Enviar pipeline para o repositório remoto:

```
git add Jenkinsfile
git commit -m "Jenkisnfile"
git push
```

### Configurar jenkins para build automático ao receber updates no gitlab

No jenkins exeute os passos abaixo:
1. Acessar o pipeline criado na atividade anterior
2. Clicar em configure
3. Em Build Triggers habilitar a opção Build when a change is pushed to GitLab. GitLab webhook URL: `URL_DO_PROJETO_NO_JENKINS`
4. Clicar em `Advanced` e em `Generate` para gerar o `Secret Token`.

No Gitlab execute os passos abaixo:
1. Faça login no gitlab, se já não estiver logado.
2. Acesse o projeto app.
3. Acesse o menu `Settings > Integrations`
4. Em `URL` insira a URL que aparece no Jenkins como webhook do pipeline
5. Em `Secret Token` insira o token gerado no Jenkins para esta conexão
6. Em `Trigger` habilite os eventos de `Push` insira `master` no campo de texto informando o branch que irá enviar os eventos de push para o Jenkins
7. Role a página para baixo e verifique se a opção `Enable SSL verfication` está habilitada.
8. Por fim clique em `Add webhook`
9. Envie um evento de teste de Push clicando em `Test` logo ao lado do hook recém criado e verificque no Jenkins se o pipeline foi disparado com a mensagem `Started by Gitlab push by USUARIO`

### Slack - Integrations

#### Etapas não serão executadas, apenas descrevem brevemente como deve feito
1. Criar conta no Slack
2. Adcionar app do Jenkins ci e escolher canal
3. Seguir passo-a-passo do site do Slack
4. Efetuar teste de conexão depois de configurado o canal

#### Exemplo de pipeline para enviar notificação no slack

1. Mover Jenkinsfile para Jenkinsfile.old
2. Criar novo arquivo Jenkinsfile `touch Jenkinsfile`
3. Copiar o conteúdo abaixo no novo Jenkinsfile:

```
node {
    stage("speak") {
        slackSend color: '#BADA55', message: 'Hello, World!', channel: '#jenkins'
    }
}

```

4. Enviar alterações:

```
git add Jenkinsfile
git commit -m "Jenkisnfile"
git push
```

5. Executar um buiild no novo pipeline
