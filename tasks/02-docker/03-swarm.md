# Criando o Cluster Swarm

`docker swarm init`

O comando acima irá gerar uma saída de comando join copie o comando e cole no segundo terminal para adicionar o servidor no cluster swarm:

`docker swarm join --token TOKEN_CLUSTER_SWARM IP_CLUSTER:2377`

## Instalando o portainer

`curl -L https://downloads.portainer.io/portainer-agent-stack.yml -o portainer-agent-stack.yml`

`docker stack deploy -c portainer-agent-stack.yml portainer`

## Verificando stack e serviços do Portainer:

`docker stack ls`

`docker service ls`

## Acessando o Portainer pela primeira vez

Clicar `+` e selecionar a opção `Select the port to view on host1`

Informar a porta 9000 e clique em `Display Port`

No primeiro acesso o Portainer irá solciitar a criação do usuário admin, deve-se inserir uma senha de no mínimo 8 dígitos e clicar em `Create user`

## Rodando nosso primeiro stack (Votting App)

`docker stack deploy -c arquivo.yml voting`

## Verificando o stack e services votting

`docker stack ls`

`docker service ls`

Verficar recursos usando o Portainer
