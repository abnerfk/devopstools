# Fluxo Gitflow

* Criar projeto com nome gitflow no Gitlab (fizemos via terraform)

## Criar arquivo README.md para inicializar o seu repositório

```
git clone URL_DO_SEU_REPO
cd gitflow
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
cd ~
```

## Exercício

Clonar repositório gitflow

```
cd git/
git clone URL_DO_SEU_REPO
```

Acessar o diretório gitflow e verificar o status dos arquivos via git:

`git status`{{execute}}


Fluxo simplificado de como utilizar o git-flow:

1-Fazer checkout do master: `git checkout master`{{execute}}

2-Criar uma branch develop: `git checkout -b develop`{{execute}}

3-Fazer push da branch develop para o repositório remoto:`git push -u origin develop`{{execute}}

4-Executar o git flow init para configurar o repositório: `git flow init`{{execute}}

Criar uma feature branch baseada na branch de develop:

```
git checkout develop
git flow feature start feature_branch
```{{execute}}

Crie um arquivo com o conteúdo abaixo na feature_branch:

`echo "Super ultra mega bacana feature." > feature.txt`{{execute}}

Efetue commit do arquivo, efetue commit no repo local e envie para o repo remoto:

```
git add .
git commit -m "GitFlow: New feature"
git push -u origin "feature/feature_branch"
```{{execute}}

Finalize a feature `git flow feature finish feature_branch`{{execute}}

Para enviar a versão atualizada da branch develop efetuar um `git push`{{execute}}

Criando uma release branch para lançamento de uma nova versão:

```
git flow release start 0.1.0
git hist
git branch
```{{execute}}

Após as adições de funcionalidades necessárias a release deve ser encerrada e integrada a master e a develop:

`git flow release finish '0.1.0'`{{execute}}

Verificar o repo local com o `git hist`{{execute}}

Enviar para o repositório central com `git push`{{execute}}

Mudar para a branch master e efetuar o push também:

```
git checkout master
git push
```{{execute}}
