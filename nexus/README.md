## Rodar Nexus usando Docker

`docker run -d -p 8081:8081 -p 18440:18440 -p 18441:18441 --name nexus sonatype/nexus3`

## Verificar o status de inicialização

`docker logs -f nexus`

Quando a mensagem abaixo surgir acessar a porta 8081 do host usando o ícone `+` e selecionar `Select port to open on host1`

```
-------------------------------------------------

Started Sonatype Nexus OSS 3.30.0-01

-------------------------------------------------
```

## Acessar diretório do volume onde os dados do Nexus são gerados

`cd $(docker inspect nexus | jq -r .[].Mounts[].Source)`

## Senha inicial do usuário admin

`cat admin.password; echo ""`

## Dentro do Nexus, via interface web:

Seguir passos descritos nos slides para criação do repositórios e demais configuraçãoes

## Adicionar repositório na configuração do Docker

Editar o arquivo /etc/docker/daemon.json e colocar os repos conforme exemplo:

```
{
"insecure-registries": ["URL-PORTA-18441"],
"registry-mirrors": ["http://URL-PORTA-18440"]
}
```

Recarregar configuração do Docker

`systemctl reload docker`

## Verificar docker info

`docker info | egrep -A 3 "(Insecure Registries|Registry Mirrors)"`

## Fazendo login no registry do Nexus

`docker login URL-PORTA-18441`

## Fazendo build ou tag de imagem para enviar para o repositŕio do Nexus

`docker tag nginx URL-PORTA-18441/docker-snapshot/nginx`

## Fazendo push da imagem para o repositório do Nexus

`docker push URL-PORTA-18441/docker-snapshot/nginx`

## Verificar a imagem:

Olhar interface Nexus se a imgem está no repositório
