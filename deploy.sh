#!/usr/bin/env bash
echo "updating system..."
sudo apt-get update -qq > /dev/null 2>&1 
echo "installing docker..."
sudo curl -sSL https://get.docker.com | bash
sudo usermod -aG docker vagrant
sudo apt-get -y install htop git-flow
echo "installing docker-compose..."
sudo curl -sL "https://github.com/docker/compose/releases/download/1.29.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
echo "installing tfenv..."
git clone https://github.com/tfutils/tfenv.git /home/vagrant/.tfenv
echo 'export PATH="$HOME/.tfenv/bin:$PATH"' >> /home/vagrant/.bashrc
chown vagrant:vagrant -R /home/vagrant/.tfenv
mkdir git
echo "done. 😎"
