resource "docker_image" "nginx" {
  name = "nginx:1.19-alpine"
}

resource "docker_container" "nginx-server" {
  name  = "web"
  image = docker_image.nginx.latest
  ports {
    internal = 80
    external = 8081
  }
  volumes {
    container_path = "/usr/share/nginx/html"
    host_path      = "/tmp/tutorial/www"
    read_only      = true
  }
  depends_on = [docker_image.nginx]
}

resource "local_file" "index_html" {
  #content         = "<html><head><title>teste</title></head><body><h1> teste teste teste </h1></body></html>"
  content         = data.template_file.index_html.rendered
  filename        = "/tmp/tutorial/www/index.html"
  file_permission = "0644"
}
