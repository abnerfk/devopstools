# run runner

```
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

# register runner

`docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register`


# rodar segundo runner

```
docker run -d --name gitlab-runner-2 --restart always \
  -v /srv/gitlab-runner-2/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

# registrar segundo runner

`docker run --rm -it -v /srv/gitlab-runner-2/config:/etc/gitlab-runner gitlab/gitlab-runner register`

# watch runners

`watch docker ps -a`
