# Setting up IAM policies for the ALB Ingress Controller in EKS with Terraform

You can provision an EKS cluster with the right policies for the ALB Ingress Controller with:

```bash
terraform init
terraform plan
terraform apply
```

It might take a while for the cluster to be creates (up to 15-20 minutes).

As soon as cluster is ready, you should find a `kubeconfig_my-cluster` kubeconfig file in the current directory.

You can use the kubeconfig file to deploy the ALB Ingress controller with:

```bash
export KUBECONFIG=${PWD}/kubeconfig_my-cluster
helm install ingress incubator/aws-alb-ingress-controller \
  --set autoDiscoverAwsRegion=true \
  --set autoDiscoverAwsVpcID=true \
  --set clusterName=my-cluster
```

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/26717437/terraform/state/infra" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/26717437/terraform/state/infra/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/26717437/terraform/state/infra/lock" \
    -backend-config="username=user" \
    -backend-config="password=PTA" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"

criar pta no projeto.

aws eks --region us-east-1 update-kubeconfig --name my-cluster
kubectl create ns cicd
helm install --namespace cicd gitlab-runner -f values.yaml gitlab/gitlab-runner
